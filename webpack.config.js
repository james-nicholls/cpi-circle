const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
// const OfflinePlugin = require('offline-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const { DefinePlugin } = require('webpack')

console.log(process.env.ENV)

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'build.js'
  },
  resolve: {
    extensions: ['.vue', '.js'],
    alias: {
      vue$: 'vue/dist/vue.esm.js'
    }
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract([
          'css-loader',
          'postcss-loader'
        ])
      }, {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }, {
        test: /\.(png|jpg|mp4|eot|otf|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: { limit: 8192 }
          }
        ]
      }
    ]
  },
  plugins: [
    new DefinePlugin({
      'env': process.env.ENV
    }),

    // new UglifyJsPlugin(),

    new ExtractTextPlugin('styles.css'),

    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],
  devtool: 'source-map',
  devServer: {
    clientLogLevel: 'info',
    host: '0.0.0.0',
    disableHostCheck: true,
    historyApiFallback: true
  }
}
