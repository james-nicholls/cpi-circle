module.exports = {
  plugins: {
    'postcss-cssnext': {
      features: {
        customProperties: {
          preserve: true,
          variables: {
            'clr-blue': '#0084D6',
            'clr-yellow': '#FCD027',
            'clr-grey': '#535353',
            'clr-white': '#E6E6E6',
            'clr-coal': '#403F3F',
            'clr-light-grey': '#A1A1A1',
            'border': '4px',
            'rads': '8px'
          }
        }
      }
    }
    // 'cssnano': {}
  }
}
