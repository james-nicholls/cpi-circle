import CpiCircle from '../../components/cpi-circle'
import CpiStatic from '../../components/cpi-static'
import CircleButton from '../../components/circle-button'
import VueHtmlVideo, { vueSource } from '@/components/vue-html-video'
import PageTitle from '../../components/home-title'
import './home-main.css'

const DELAY = 500

const PAGE_DATA = (root = '.') => [
  {
    title: 'Diagnostics',
    href: `${root}/videos/v5/diagnostics/diagnostics.mp4`,
    links: {
      'Diagnostics': `${root}/videos/v5/diagnostics/diagnostics.mp4`
    },
    slides: {
      'First': `${root}/slides/diagnostics/Diagnostics1.jpg`,
      'Second': `${root}/slides/diagnostics/Diagnostics2.jpg`,
      'Third': `${root}/slides/diagnostics/Diagnostics3.jpg`,
      'Fourth': `${root}/slides/diagnostics/Diagnostics4.jpg`,
      'Fith': `${root}/slides/diagnostics/Diagnostics5.jpg`,
      'Sixth': `${root}/slides/diagnostics/Diagnostics6.jpg`,
      'Seventh': `${root}/slides/diagnostics/Diagnostics7.jpg`,
      'Eighth': `${root}/slides/diagnostics/Diagnostics8.jpg`,
      'Ninth': `${root}/slides/diagnostics/Diagnostics9.jpg`
    }
  }, {
    title: 'Mobile',
    href: `${root}/videos/v7/mobile/mobile.mp4`,
    links: {
      'Mobile': `${root}/videos/v7/mobile/mobile.mp4`
    },
    slides: {
      'First': `${root}/slides/mobile/Mobile1.jpg`,
      'Second': `${root}/slides/mobile/Mobile2.jpg`,
      'Third': `${root}/slides/mobile/Mobile3.jpg`,
      'Fourth': `${root}/slides/mobile/Mobile4.jpg`,
      'Fith': `${root}/slides/mobile/Mobile5.jpg`,
      'Sixth': `${root}/slides/mobile/Mobile6.jpg`,
      'Seventh': `${root}/slides/mobile/Mobile7.jpg`,
      'Eighth': `${root}/slides/mobile/Mobile8.jpg`
    }
  }, {
    title: 'Offline Cashless',
    href: `${root}/videos/v7/offline-cashless/offline-cashless.mp4`,
    links: {
      'offline-cashless': `${root}/videos/v7/offline-cashless/offline-cashless.mp4`
    },
    slides: {
      'First': `${root}/slides/offline-cashless/CPI-AdvancePayment1.jpg`,
      'Second': `${root}/slides/offline-cashless/CPI-AdvancePayment2.jpg`,
      'Third': `${root}/slides/offline-cashless/CPI-AdvancePayment3.jpg`,
      'Fourth': `${root}/slides/offline-cashless/CPI-AdvancePayment4.jpg`,
      'Fith': `${root}/slides/offline-cashless/CPI-AdvancePayment5.jpg`,
      'Sixth': `${root}/slides/offline-cashless/CPI-AdvancePayment6.jpg`,
      'Seventh': `${root}/slides/offline-cashless/CPI-AdvancePayment7.jpg`
    }
  }, {
    title: 'Security',
    href: `${root}/videos/v7/security/security.mp4`,
    links: {
      'Security': `${root}/videos/v7/security/security.mp4`
    },
    slides: {
      'First': `${root}/slides/security/Security1.jpg`,
      'Second': `${root}/slides/security/Security2.jpg`,
      'Third': `${root}/slides/security/Security3.jpg`,
      'Fourth': `${root}/slides/security/Security4.jpg`,
      'Fith': `${root}/slides/security/Security5.jpg`,
      'Sixth': `${root}/slides/security/Security6.jpg`,
      'Seventh': `${root}/slides/security/Security7.jpg`,
      'Eighth': `${root}/slides/security/Security8.jpg`,
      'Ninth': `${root}/slides/security/Security9.jpg`,
      'Tenth': `${root}/slides/security/Security10.jpg`,
      'Eleventh': `${root}/slides/security/Security11.jpg`
    }
  }, {
    title: 'Credit',
    href: `${root}/videos/v5/credit/credit.mp4`,
    links: {
      Credit: `${root}/videos/v5/credit/credit.mp4`
    },
    slides: {
      'First': `${root}/slides/credit/Credit1.jpg`,
      'Second': `${root}/slides/credit/Credit2.jpg`,
      'Third': `${root}/slides/credit/Credit3.jpg`,
      'Fourth': `${root}/slides/credit/Credit4.jpg`,
      'Fith': `${root}/slides/credit/Credit5.jpg`,
      'Sixth': `${root}/slides/credit/Credit6.jpg`,
      'Seventh': `${root}/slides/credit/Credit7.jpg`,
      'Eighth': `${root}/slides/credit/Credit8.jpg`
    }
  }, {
    title: 'Cash',
    href: `${root}/videos/v7/cash/cash.mp4`,
    links: {
      'Cash': `${root}/videos/v7/cash/cash.mp4`,
      'Talos': `${root}/videos/v7/cash/cash-talos.mp4`,
      'Gryphon': `${root}/videos/v7/cash/cash-gryphon.mp4`
    },
    slides: {
      'First': `${root}/slides/cash/Cash1.jpg`,
      'Second': `${root}/slides/cash/Cash2.jpg`,
      'Third': `${root}/slides/cash/Cash3.jpg`,
      'Fourth': `${root}/slides/cash/Cash4.jpg`,
      'Fith': `${root}/slides/cash/Cash5.jpg`,
      'Sixth': `${root}/slides/cash/Cash6.jpg`,
      'Seventh': `${root}/slides/cash/Cash7.jpg`,
      'Eighth': `${root}/slides/cash/Cash8.jpg`
    }
  }
]

const HomeMain = {
  components: {
    'cpi-circle': CpiCircle,
    'cpi-static': CpiStatic,
    'vue-html-video': VueHtmlVideo,
    'vue-source': vueSource,
    'page-title': PageTitle,
    'circle-button': CircleButton
  },
  data () {
    return {
      active: false,
      bg: `${this.BASE_URL}/videos/v2/background.mp4`,
      video: '',
      slides: {}
    }
  },
  methods: {
    navigate (evt, opts) {
      if (this.MOBILE && evt === 'forward') evt = 'slides'

      return ({
        slides: ({ slides }) => {
          this.active = true
          this.video = ''
          this.slides = slides
        },
        forward: ({ link }) => {
          this.active = true
          setTimeout(() => {
            this.video = (link || this.bg)
          }, DELAY)
        },
        back: () => {
          this.active = false
          this.video = this.bg
        },
        change: ({ link, slides }) => {
          this.video = link
          this.slides = slides
        }
      }[evt])(opts)
    }
  },
  created () {
    this.navigate('change', { link: this.bg })
  },
  render (vc) {
    return vc('section', {
      attrs: {
        id: 'home-page'
      }
    }, [
      vc('vue-html-video', {
        props: {
          id: 'bg-video-content',
          poster: '',
          autoplay: true,
          background: true,
          loop: !this.active,
          sources: [
            this.video
          ]
        }
      }),

      // Controls navigations button placemenet and animations
      vc('cpi-circle', {
        on: {
          circle: ({ navigation, link, slides }) => {
            this.navigate(navigation, { link, slides })
          }
        },
        props: {
          content: PAGE_DATA(this.BASE_URL),
          title: this.TITLE
        }
      }, [

      ]),

      vc('transition', {
        props: {
          name: 'cpi-static-fade'
        }
      }, [
        this.MOBILE && this.active &&
        vc('cpi-static', {
          props: {
            content: this.slides
          }
        }, [

        ])
      ])
    ])
  }
}

export default HomeMain
export {

}
