/* global history */

import NavBar, { navBarTile, navBarLogo } from '@/components/nav-bar'
import HomeMain from './pages/home-main/index.js'

import './reset.css'
import './vars.css'
import './app.css'

const DATA_ROOT = 'http://cpitouch.prototype.cx' // http://cpitouch.prototype.cx

const App = {
  data () {
    return {
      mobs: this.MOBILE
    }
  },
  components: {
    'nav-bar': NavBar,
    'nav-bar-tile': navBarTile,
    'nav-bar-logo': navBarLogo,
    'home-main': HomeMain
  },
  render (vc) {
    return vc('main', {
      attrs: { id: 'App' }
    }, [
      vc('home-main'),

      vc('nav-bar', {
        props: {
          mobile: this.MOBILE,
          side: 'bottom'
        }
      }, [
        vc('nav-bar-logo', {
          props: {
            link: '/v2/',
            icon: `url(${require('@/assets/icons/mojs/cpi-logo-clr.svg')})`
          }
        }),

        vc('nav-bar-tile', {
          props: {
            title: 'Platform',
            icon: `url(${require('@/assets/icons/mojs/sync-o.svg')})`,
            link: ``
          },
          on: {
            nav: evt => {
              history.go(0)
              // this.$bus.$emit('nav', evt)
            }
          }
        }),

        vc('nav-bar-tile', {
          props: {
            title: 'Innovation',
            icon: `url(${require('@/assets/icons/mojs/cpi-logo.svg')})`,
            link: `${DATA_ROOT}/videos/v4/innovation/innovation.mp4`,
            slides: {
              'First': `${DATA_ROOT}/slides/story/Story1.jpg`,
              'Second': `${DATA_ROOT}/slides/story/Story2.jpg`,
              'Third': `${DATA_ROOT}/slides/story/Story3.jpg`,
              'Fourth': `${DATA_ROOT}/slides/story/Story4.jpg`,
              'Fith': `${DATA_ROOT}/slides/story/Story5.jpg`,
              'Sixth': `${DATA_ROOT}/slides/story/Story6.jpg`,
              'Seventh': `${DATA_ROOT}/slides/story/Story7.jpg`,
              'Eighth': `${DATA_ROOT}/slides/story/Story8.jpg`,
              'Ninth': `${DATA_ROOT}/slides/story/Story9.jpg`,
              'Tenth': `${DATA_ROOT}/slides/story/Story10.jpg`,
              'Eleventh': `${DATA_ROOT}/slides/story/Story11.jpg`,
              'Twelth': `${DATA_ROOT}/slides/story/Story12.jpg`,
              'Thirteenth': `${DATA_ROOT}/slides/story/Story13.jpg`,
              'Fourteenth': `${DATA_ROOT}/slides/story/Story14.jpg`
            }
          },
          on: {
            nav: evt => {
              this.$bus.$emit('nav', evt)
            }
          }
        }),

        vc('nav-bar-tile', {
          props: {
            title: 'CCS',
            icon: `url(${require('@/assets/icons/mojs/ccs.svg')})`,
            link: `${DATA_ROOT}/videos/v7/ccs/ccs.mp4`,
            slides: {
              'First': `${DATA_ROOT}/slides/ccs/CCS1.jpg`,
              'Second': `${DATA_ROOT}/slides/ccs/CCS2.jpg`,
              'Third': `${DATA_ROOT}/slides/ccs/CCS3.jpg`,
              'Fourth': `${DATA_ROOT}/slides/ccs/CCS4.jpg`,
              'Fith': `${DATA_ROOT}/slides/ccs/CCS5.jpg`,
              'Sixth': `${DATA_ROOT}/slides/ccs/CCS6.jpg`,
              'Seventh': `${DATA_ROOT}/slides/ccs/CCS7.jpg`,
              'Eighth': `${DATA_ROOT}/slides/ccs/CCS8.jpg`
            }
          },
          on: {
            nav: evt => {
              this.$bus.$emit('nav', evt)
            }
          }
        }),

        vc('nav-bar-tile', {
          props: {
            title: 'LAT.AM',
            icon: `url(${require('@/assets/icons/mojs/latam.svg')})`,
            link: `${DATA_ROOT}/videos/v7/latam/latam.mp4`,
            slides: {
              'First': `${DATA_ROOT}/slides/latam/LATAM1.jpg`,
              'Second': `${DATA_ROOT}/slides/latam/LATAM2.jpg`,
              'Third': `${DATA_ROOT}/slides/latam/LATAM3.jpg`,
              'Fourth': `${DATA_ROOT}/slides/latam/LATAM4.jpg`,
              'Fith': `${DATA_ROOT}/slides/latam/LATAM5.jpg`,
              'Sixth': `${DATA_ROOT}/slides/latam/LATAM6.jpg`,
              'Seventh': `${DATA_ROOT}/slides/latam/LATAM7.jpg`,
              'Eighth': `${DATA_ROOT}/slides/latam/LATAM8.jpg`,
              'Ninth': `${DATA_ROOT}/slides/latam/LATAM9.jpg`,
              'Tenth': `${DATA_ROOT}/slides/latam/LATAM10.jpg`
            }
          },
          on: {
            nav: evt => {
              this.$bus.$emit('nav', evt)
            }
          }
        }),

        false &&
        vc('nav-bar-tile', {
          props: {
            active: this.mobs,
            title: ['', ''],
            icon: ``,
            link: ``,
            mobile: !this.mobs
          },
          on: {
            nav: evt => {
              this.mobs = !this.mobs
              this.$bus.$emit('nav', evt)
            }
          }
        })
      ])
    ])
  }
}

export default App
export {

}
