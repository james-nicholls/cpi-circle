/* global fetch env dvc */

import Vue from 'vue'

import App from './app.js'

import Globals from './globals.js'

// NOTE Disable Safari bounce
document.ontouchmove = e => { e.preventDefault() }
document.ongesturestart = e => { e.preventDefault() }

// NOTE Register the Global Event bus for event tracking and handeling
const EventBus = new Vue({
  data () {
    return {
      timing: undefined
    }
  },
  created () {
    // Event logging middleware
    this.$on('nav', this['log']) // Logs events from the navigation bar`
    this.$on('tile', this['log']) // Logs events from the main tiles
    this.$on('video', this['log']) // Logs the events from the video player

    this.$on('video', this['timer'])
  },
  methods: {
    track (evt) {
      window.dataLayer = window.dataLayer || []
      window.dataLayer.push(evt)
    },
    timer ({evt = 'begin'}) {
      ({
        begin: () => {
          console.log('Reset!')
          clearTimeout(this.timing)
        },
        end: () => {
          console.log('Ended...')
          this.timing = setTimeout(t => {
            console.log('Timeout!')
            this.$emit('timeout')
          }, this['TIMEOUT'])
        }
      }[evt])()
    },
    log (event) {
      this['track'](event)
      console.log({[Date.now()]: event})
      fetch('/', {
        method: 'POST',
        body: JSON.stringify({
          [Date.now()]: event
        })
      })
      // .then(console.log)
    }
  }
})

const GLOBALS = Globals({ env })

console.log(GLOBALS)

// Addvs environment variables to the Vue prototype for global access
Object.keys(GLOBALS).forEach(key => {
  Vue.prototype[key] = GLOBALS[key]
})

Object.defineProperties(Vue.prototype, {
  $bus: { get: () => EventBus }
})

/* eslint-disable no-new */
new Vue({
  el: '#root',
  components: {
    app: App
  },
  render (vc) {
    return vc('app')
  }
})
