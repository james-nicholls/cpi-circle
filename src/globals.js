import BrowserDetect from 'browser-detect'

const { mobile, name } = BrowserDetect()

const Globals = ({env = 'prod'}) => ({
  // Defaults
  TIMEOUT: 10000,
  BASE_URL: '.',
  MOBILE: mobile,
  IE: (name === 'ie' || name === 'edge'),
  ...({
    production: {
      PRODUCTION: true,
      BASE_URL: '.'
    },
    dev: {
      DEVELOPMENT: true,
      BASE_URL: 'http://cpitouch.prototype.cx',
      MOBILE: true,
      TIMEOUT: 10000
    }
  }[env]),
  ...({
    desktop: {
      TITLE: 'Select a Topic to Start'
    },
    mobile: {
      TITLE: 'Touch a Topic to Start'
    }
  }[mobile ? 'mobile' : 'desktop'])
})

export default Globals
