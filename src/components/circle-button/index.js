const STYLE_ACTIVE = active => active
  ? {
    'background-color': '#0084D6',
    border: '4px solid #E6E6E6'
  }
  : {
    'background-color': 'white',
    border: '4px solid #0084D6'
  }

const CircleButton = {
  functional: true,
  render (vc, { props, listeners }) {
    const { title = ['Title'], active = false, ...content } = props
    const { pressed } = listeners

    return vc('div', {
      on: {
        click: () => { pressed(content) }
      },
      attrs: {

      },
      style: {
        display: 'flex',
        'justify-content': 'center',
        'align-items': 'center',
        width: '100%',
        height: '100%',
        color: '#535353',
        'border-radius': '9999px',
        transition: `
          background-color 500ms ease 0s,
          border 500ms ease 0s
        `,
        ...STYLE_ACTIVE(active)
      }
    }, [
      vc('h4', {}, [
        active ? (title[1] || title[0]) : title[0]
      ])
    ])
  }
}

export default CircleButton
export {

}
