import './nav-bar.css'

const navBarTile = {
  functional: true,
  render (vc, { props, children, listeners }) {
    const { title, icon, active, ...content } = props
    const { nav } = listeners
    const [ main, second ] = [].concat(title)

    return vc('div', {
      class: 'nav-bar-tile',
      style: {
        width: '15%'
      },
      on: {
        click: evt => {
          nav(content)
        }
      }
    }, [
      vc('div', {
        class: 'nav-bar-icon',
        style: {
          'background-image': icon
        }
      }),

      vc('h5', {}, [
        active
          ? second
          : main
      ])
    ])
  }
}

const navBarLogo = {
  functional: true,
  render (vc, { props, children }) {
    const { icon, href } = props
    return vc('a', {
      class: 'nav-bar-logo',
      attrs: {
        href
      },
      style: {
        width: '10%',
        height: '100%',
        'background-image': icon
      }
    })
  }
}

const navAlignParams = {
  top: {

  },
  right: {

  },
  bottom: {
    width: '100%',
    position: 'absolute',
    left: 0,
    bottom: 0,
    'flex-direction': 'row'
  },
  left: {
    height: '100vh',
    position: 'absolute',
    left: 0,
    top: 0,
    'flex-direction': 'column'
  }
}

const NavBar = {
  functional: true,
  render (vc, { props, children }) {
    const { side, mobile } = props
    return vc('aside', {
      style: {
        height: mobile
          ? '45px'
          : '85px',
        display: 'flex',
        position: 'absolute',
        'background-color': '#403F3F',
        ...navAlignParams[side]
      }
    }, [
      children
    ])
  }
}

export default NavBar
export {
  navBarTile,
  navBarLogo
}
