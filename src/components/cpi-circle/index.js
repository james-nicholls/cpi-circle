/* eslint-disable no-new, operator-linebreak */

import mojs from 'mo-js'
import { el, mount, setStyle } from 'redom'
// import { reverse } from 'svg-path-reverse'

import './cpi-circle.css'
import ICONS from '@/assets/icons'

const ANIM_TIME = 1500

const PATH = `M 265.905 539.867 C 265.905 690.442 386.733 812.367 536.941 812.367 C 625.45 812.367 703.676 770.132 753.255 704.761 C 783.37 665.097 829.645 641.961 878.855 641.961 L 879.594 641.961 C 932.715 641.983 982.855 666.512 1015.479 708.435 C 1065.424 771.973 1142.915 813.106 1229.955 813.106 C 1380.163 813.106 1503.194 690.075 1503.194 539.499 L 1503.194 539.499 L 1503.194 539.132 C 1503.194 388.557 1382.366 266.632 1232.158 266.632 C 1143.649 266.632 1065.423 308.867 1015.844 374.238 C 985.729 413.902 939.455 437.038 890.244 437.038 L 889.509 437.038 C 836.388 437.016 786.248 412.487 753.624 370.564 C 703.675 307.026 626.184 265.893 539.144 265.893 C 388.936 265.893 265.905 388.924 265.905 539.5 L 265.905 539.5 L 265.905 539.867 Z`

const FADE_CSS = faded => ({
  transition: faded
    ? `opacity ${ANIM_TIME / 2}ms ${ANIM_TIME / 2}ms`
    : `opacity ${ANIM_TIME / 2}ms`,
  opacity: faded
    ? 1
    : 0
})

Object.keys(ICONS).map(ico => {
  mojs.addShape(ico, class extends mojs.CustomShape {
    getShape () {
      return ICONS[ico].reduce((temp, path) => {
        return (temp += `<path d="${path}" />`)
      }, ``)
    }
  })
})

const textKebab = text =>
  text
    .toLowerCase()
    .replace(' ', '-')

const navigationButton = {
  functional: true,
  render (vc, { props, listeners }) {
    const { press } = listeners
    const {
      title = 'Home',
      active = false,
      link = '/'
    } = props

    return vc('div', {
      on: {
        click: () => press(link)
      },
      class: `cpi-nav-btn ${active && 'active'}`
    }, [
      vc('h4', {

      }, [
        title
      ])
    ])
  }
}

const circleMain = ({ parent, x = 0, y = 0, angle, mobile, delay = 0, icon = '', text, ...rest }, cb) => {
  const style = {
    mobile: {
      radius: '60',
      font: '65%',
      icon: 0.4
    },
    desktop: {
      radius: '100',
      font: '',
      icon: 0.65
    }
  }[mobile ? 'mobile' : 'desktop']

  const innerRads = style.radius - 4
  let textOpacity = 1

  const outer = new mojs.Shape({
    parent,
    shape: 'circle',
    fill: '#FCD027',
    radius: {[style.radius]: 0},
    left: '0%',
    top: '0%',
    x,
    y,
    angle,
    isShowStart: true,
    //
    delay,
    duration: 250
  }).then({
    duration: ANIM_TIME / 3
  })

  const inner = new mojs.Shape({
    parent,
    shape: 'circle',
    fill: 'white',
    radius: {[innerRads]: 0},
    left: '0%',
    top: '0%',
    x,
    y,
    angle,
    isShowStart: true,
    //
    delay: delay - 100,
    duration: 250,
    //
    onStart (isForward) {
      setStyle(this.el.querySelector('h5'), {
        opacity: isForward
          ? 0
          : 1
      })
    }
  }).then({
    duration: ANIM_TIME / 3
  })

  const ico = new mojs.Shape({
    parent,
    shape: textKebab(text),
    fill: '#0084D6',
    left: '0%',
    top: '-20px',
    scale: {[style.icon]: 0},
    x,
    y,
    angle,
    isShowStart: true,
    //
    delay,
    duration: 250
  }).then({
    duration: ANIM_TIME / 3
  })

  ;[ico, inner, outer].map(elm => (elm.el.onclick = () => cb(rest)))
  // inner.el.onclick = () => cb(link)

  mount(inner.el, el('h5', {
    style: {
      opacity: textOpacity,
      'margin-top': '-35%',
      'text-align': 'center',
      color: '#0084D6',
      'font-size': style.font
    }
  }, text))

  return [ inner, outer, ico ]
}

const SvgPathPointTangent = (path, length, dp = 2) => {
  const { x: x1, y: y1 } = path.getPointAtLength(length)
  const { x: x2, y: y2 } = path.getPointAtLength(length + dp)

  return Math.atan2(y1 - y2, x1 - x2) * (180 / Math.PI)
}

const SvgPathPointsEqual = (path, points) => {
  const { a: scale } = path.getCTM()
  const length = path.getTotalLength()
  const segment = length / points

  return [...Array(points)].map((_, i) => {
    const { x, y } = path.getPointAtLength(segment * i)
    const angle = SvgPathPointTangent(path, segment * i)

    return {
      x: (x * scale),
      y: (y * scale),
      a: angle
    }
  })
}

const CpiCircle = {
  data () {
    return {
      length: 0,
      active: false,
      spinner: {},
      anim: {},
      circles: [],
      wait: false,
      links: {},
      current: '',
      halos: [],
      ended: false
    }
  },
  props: {
    content: {
      type: Array
    },
    points: {
      type: Number,
      default: 6
    },
    title: {
      type: String,
      default: 'Title'
    }
  },
  methods: {
    navigate (nav = 'forward', opts = {}) {
      if (this.wait) return
      else this.wait = true

      this.ended = false

      ;({
        end: () => {
          this.ended = true
        },
        forward: ({ link = '', slides = {}, links = {} }) => {
          if (!link) return

          this.anim.play()

          this.active = true
          this.current = link
          this.links = links
          this.$emit('circle', {
            navigation: 'forward',
            link,
            slides
          })
        },
        change: ({ link = '', slides = {}, links = {} }) => {
          this.spinner.play()

          this.current = link
          this.links = links
          this.$emit('circle', {
            navigation: 'change',
            link,
            slides
          })
        },
        back: ({ link = '' }) => {
          this.active = false
          this.wait = false
          this.anim
            .playBackward()
          this.$emit('circle', {
            navigation: 'back',
            link
          })
        }
      }[nav])(opts)

      setTimeout(() => {
        this.wait = false
        return true
      }, ANIM_TIME)
    }
  },
  created () {
    this.$bus.$on('nav', ({ link, slides }) => {
      if (this.active) this.navigate('change', { link, slides })
      else this.navigate('forward', { link, slides })
    })

    this.$bus.$on('timeout', evt => {
      this.navigate('back')
    })

    this.$bus.$on('video', ({ evt }) => {
      if (evt === 'end') this.navigate('end')
    })
  },
  mounted () {
    if (!this.length) this.length = this.$refs['circle-path'].getTotalLength()

    const circlePoints = SvgPathPointsEqual(this.$refs['circle-path'], this.points)

    // Centers SVG loop and sets responsive attributes
    new mojs.Html({
      el: this.$refs['svg-wrapper'],
      position: 'absolute',
      'width': '1770px',
      'height': '1080px',
      'max-width': '100%',
      'max-height': '60vw',
      top: '45%',
      left: '50%',
      x: '-50%',
      y: '-50%'
    })

    this.spinner = new mojs.Shape({
      parent: this.$refs['svg-wrapper'],
      shape: 'sync',
      fill: 'none',
      stroke: '#FCD027',
      strokeWidth: 4,
      opacity: { 0: 1 },
      //
      duration: 250
    }).then({
      angle: { 0: 360 },
      //
      duration: 1000,
      repeat: 2,
      easing: 'expo.inout'
    }).then({
      opacity: { 1: 0 },
      //
      duration: 250
    })

    this.circles = circlePoints.map(({ x, y }, i) => { // TODO Abstract functionality
      return circleMain({
        parent: this.$refs['svg-wrapper'],
        x: x,
        y: y,
        mobile: this.MOBILE,
        delay: ANIM_TIME - ((ANIM_TIME / this.points) * i),
        icon: this.content[i].icon,
        text: this.content[i].title,
        link: this.content[i].href,
        links: this.content[i].links,
        slides: this.content[i]['slides']
      }, ({ link = '', links = {}, slides = {} }) => {
        // TODO Remove navigation logic
        if (!this.active && Object.keys(links).length) { // Only allow click handle for links
          this.links = links
          this.navigate('forward', {
            link,
            links,
            slides
          })
        }
      })
    })

    this.halos = circlePoints.map(({ x, y }, i) => {
      const halo = new mojs.Shape({
        parent: this.$refs['svg-wrapper'],
        shape: 'circle',
        stroke: '#FCD027',
        strokeWidth: { 12: 0 },
        fill: 'none',
        radius: this.MOBILE
          ? { 65: 120 }
          : { 90: 200 },
        left: '0%',
        top: '0%',
        x,
        y,
        //
        easing: 'expo.inout',
        delay: 'rand(5000, 15000)',
        duration: 500,
        repeat: 9999,
        onStart () {
          this.generate()
        }
      })

      setStyle(halo.el, {
        pointerEvents: 'none'
      })

      return halo
    })

    this.anim = new mojs.Timeline()
      .add(this.circles)
  },
  updated () {
    if (this.active) {
      this.halos.forEach(halo => { halo.stop() })

      !this.ended &&
        this.spinner.play()
    } else {
      this.halos.forEach(halo => { halo.play() })
      this.spinner
        .playBackward()
        .stop()
    }
  },
  render (vc) {
    return vc('div', {
      attrs: {
        id: 'cpi-circle'
      }
    }, [
      vc('h2', {
        style: {
          ...FADE_CSS(!this.active),
          'font-size': this.MOBILE
            ? '125%'
            : '',
          color: '#FCD027',
          position: 'absolute',
          top: '45%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          'z-index': 1
        }
      }, [
        this.title
      ]),

      vc('div', {
        ref: 'svg-wrapper',
        attrs: {
          id: 'svg-wrapper'
        }
      }, [
        vc('svg', {
          attrs: {
            viewBox: '0 0 1770 1080', // FIXME Static sizing
            width: '1770', // `${1770 / 1.15}`
            height: '1080' // `${1080 / 1.15}`
          },
          style: {
            position: 'absolute',
            width: '100%',
            height: '100%'
          }
        }, [
          vc('path', {
            ref: 'circle-path',
            style: {
              transition: `stroke-dashoffset ${ANIM_TIME}ms linear ${this.active ? 0 : (ANIM_TIME / 2)}ms`, // Transition out delay
              'stroke-dashoffset': this.active
                ? this.length
                : 0,
              'stroke-dasharray': this.length
            },
            attrs: {
              d: PATH,
              fill: 'none',
              stroke: '#FCD027',
              'stroke-width': 6
            }
          })
        ])
      ]),

      // Video end buttons
      Object.keys(this.links).length > 2 && this.ended && this.current === this.links['Cash'] &&
      vc('div', {
        attrs: {
          id: 'cpi-circle-end'
        },
        style: {
          display: 'flex',
          'justify-content': 'center',
          position: 'absolute',
          margin: 'auto',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)'
        }
      }, [
        Object.keys(this.links)
          .slice(1)
          .map((link, idx) =>
            vc(navigationButton, {
              on: {
                press: () => {
                  this.active && this.navigate('change', { link: this.links[link], links: this.links })
                }
              },
              props: {
                title: link,
                active: this.links[link] === this.current
              }
            })
          )
      ]),

      !this.MOBILE && // NOTE Remove home button for mobile site
      vc('div', {
        style: {
          ...FADE_CSS(this.active),
          display: 'flex',
          'flex-direction': 'row',
          position: 'absolute',
          bottom: '115px',
          left: '45px'
        }
      }, [
        vc(navigationButton, {
          on: {
            press: () => {
              this.active && this.navigate('back')
            }
          }
        }),

        Object.keys(this.links).length > 2 && !this.MOBILE &&
        Object.keys(this.links).map((link, idx) =>
          vc(navigationButton, {
            on: {
              press: () => {
                this.active && this.navigate('change', { link: this.links[link], links: this.links })
              }
            },
            props: {
              title: link,
              active: this.links[link] === this.current
            }
          })
        )
      ])
    ])
  }
}

export default CpiCircle
export {
  navigationButton
}
