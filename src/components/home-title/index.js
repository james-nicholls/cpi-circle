import './home-title.css'

const PageTitle = {
  functional: true,
  render (vc, { props }) {
    const { header, sub } = props
    return vc('header', {
      style: {
        margin: '16px 0',
        position: 'absolute',
        display: 'flex',
        top: 0,
        width: '100vw',
        'flex-direction': 'column',
        'justify-content': 'center',
        'align-items': 'center'
      }
    }, [
      vc('h3', {
        style: {
          color: '#E6E6E6',
          'margin-bottom': '12px',
          'padding-bottom': '12px',
          'border-bottom': '4px solid white',
          'border-color': '#0084D6'
        }
      }, [ header ]),

      vc('h3', {
        style: {
          color: '#0084D6'
        }
      }, [ sub ])
    ])
  }
}

export default PageTitle
export {

}
