// const navigationButton = {
//   functional: true,
//   render (vc, { props, listeners }) {
//     const { press } = listeners
//     const {
//       title = 'Home',
//       active = false,
//       ...content
//     } = props
//
//     return vc('div', {
//       on: {
//         click: () => press(content)
//       },
//       class: `cpi-nav-btn${active ? ' active' : ''}`
//     }, [
//       vc('h4', {
//
//       }, [
//         title
//       ])
//     ])
//   }
// }

const navigationCircle = {
  functional: true,
  render (vc, { props, listeners }) {
    const { press } = listeners
    const {
      active = false,
      ...content
    } = props

    return vc('div', {
      on: {
        click: () => press(content)
      },
      style: {
        'background-color': active
          ? '#0084D6'
          : 'white',
        display: 'block',
        width: '15px',
        height: '15px',
        border: '4px solid #0084D6',
        'border-radius': '9999px'
      }
    })
  }
}

// const navigationArrow = {
//   functional: true,
//   render (vc, { props, listeners }) {
//     const { click = () => ({}) } = listeners
//     const { right = false } = props
//
//     return vc('div', {
//       on: {
//         click
//       },
//       style: {
//         color: '#0084D6',
//         position: 'absolute',
//         top: '40%',
//         [right ? 'right' : 'left']: '0%',
//         'background-color': 'none',
//         display: 'block',
//         width: '50px',
//         height: '50px',
//         border: '4px solid #0084D6',
//         'border-radius': '9999px',
//         margin: '64px',
//         'text-align': 'center',
//         'line-height': '50px',
//         'font-weight': 'bold'
//       }
//     }, [
//       vc('h3', {}, [
//         right
//           ? '>'
//           : '<'
//       ])
//     ])
//   }
// }

const CpiStatic = {
  data () {
    return {
      current: '',
      swipe: {
        st: 0,
        end: 0,
        d: 0
      }
    }
  },
  props: {
    content: {
      type: Object,
      default: () => ({})
    }
  },
  watch: {
    content () {
      this.current = Object.keys(this.content)[0]
    }
  },
  methods: {
    transition ({ start = this.swipe.st, end = this.swipe.end, sensitivity = 100 }) {
      if (start.x - end.x > sensitivity) this.navigate('forward')
      if (start.x - end.x < -sensitivity) this.navigate('back')
    },
    touch (proc, opts) {
      ;({
        start: ({changedTouches}) => {
          this.swipe.st = (({
            clientX,
            clientY
          }) => ({
            x: clientX,
            y: clientY
          }))(changedTouches[0])
        },
        end: ({changedTouches}) => {
          this.swipe.end = (({
            clientX,
            clientY
          }) => ({
            x: clientX,
            y: clientY
          }))(changedTouches[0])

          this.transition({
            start: this.swipe.st,
            end: this.swipe.end
          })
        },
        cancel: ({changedTouches}) => {
        },
        move: ({changedTouches}) => {
        }
      }[proc])(opts)
    },
    navigate (proc, opts) {
      const vals = Object.keys(this.content)
      const i = vals.indexOf(this.current)

      ;({
        forward: () => {
          this.current = (vals[i + 1] || this.current)
        },
        back: () => {
          this.current = (vals[i - 1] || this.current)
        }
      }[proc])(opts)
    }
  },
  created () {
    this.current = Object.keys(this.content)[0]
  },
  mounted () {
    this.$el.addEventListener('touchstart', evt => this.touch('start', evt), false)
    this.$el.addEventListener('touchend', evt => this.touch('end', evt), false)
    this.$el.addEventListener('touchcancel', evt => this.touch('cancel', evt), false)
    this.$el.addEventListener('touchmove', evt => this.touch('move', evt), false)
  },
  render (vc) {
    return vc('div', {
      attrs: {
        id: 'cpi-static'
      },
      style: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: '-5vh',
        left: '0',
        'background-color': '#535353',
        'background-position': 'center center',
        'background-size': 'cover',
        'background-image': `url('${this.content[this.current]}')`
      }
    }, [
      // vc(navigationArrow, {
      //   on: {
      //     click: e => {
      //       this.navigate('back')
      //     }
      //   }
      // }),
      //
      // vc(navigationArrow, {
      //   props: {
      //     right: true
      //   },
      //   on: {
      //     click: e => {
      //       this.navigate('forward')
      //     }
      //   }
      // }),

      vc('div', {
        attrs: {
          id: 'cpi-static-nav'
        },
        style: {
          display: 'flex',
          'justify-content': 'space-around',
          padding: '32px',
          'padding-top': '83vh'
        }
      }, [
        Object.keys(this['content']).map((item, idx) => {
          return vc(navigationCircle, {
            props: {
              title: item,
              active: this.current === item
            },
            on: {
              press: ({ position }) => {
                this.current = item
              }
            }
          })
        })
      ])
    ])
  }
}

export default CpiStatic
export {

}
