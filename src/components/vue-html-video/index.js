const ANIM_DELAY = 500

const FADE_CSS = faded => ({
  '-webkit-transition': `opacity ${ANIM_DELAY}ms`,
  '-moz-transition': `opacity ${ANIM_DELAY}ms`,
  transition: `opacity ${ANIM_DELAY}ms`,
  opacity: faded ? 0 : 1
})

/**
 * [source description]
 * @param  {[type]} vc  [description]
 * @param  {[type]} src [description]
 * @return {[type]}     [description]
 */
const vueSource = {
  functional: true,
  render (vc, { props }) {
    const { src } = props
    const ext = src.split('.').pop()
    const srcFrame = type =>
      vc('source', {
        attrs: {
          src,
          type
        }
      })

    switch (ext) {
      case 'webm':
        return srcFrame('video/webm; codecs="vp8.0, vorbis"')
      case 'ogg':
        return srcFrame('video/ogg; codecs="theora, vorbis"')
      case 'mp4':
        return srcFrame('video/mp4; codecs="avc1.4D401E, mp4a.40.2"')
      default:
        return vc('p', {}, [ 'HTML5 Video does not support this content' ])
    }
  }
}

// CSS parameters for a background video object
// const BG_CSS = {
//   'position': 'fixed',
//   'top': '0px',
//   'left': '0px',
//   'min-width': '100vmin',
//   'min-height': '100vmin',
//   'max-width': 'calc(100 * (1vw + 1vh - 1vmin))',
//   'max-height': 'calc(100 * (1vw + 1vh - 1vmin))',
//   'width': 'auto',
//   'height': 'auto',
//   'z-index': -99,
//   'overflow': 'hidden'
// }

const BG_CSS = {
  // 'width': '100%',
  // 'height': '100%',
  // 'object-fit': 'cover',
  // 'z-index': -99,
  position: 'absolute', // HACK Hats off to me :D
  'min-width': 'calc(100 * (1vw + 1vh - 1vmin))',
  height: 'calc(100 * (1vw + 1vh - 1vmin))',
  transform: 'translate(-50%, -50%)',
  top: '50%',
  left: '50%',
  'z-index': '-99'
}

/**
 * [HtmlVideo description]
 * @type {Object}
 */
const VueHtmlVideo = {
  data () {
    return {
      changing: false
    }
  },
  props: {
    id: {
      type: String,
      defualt: 'video-element'
    },
    poster: {
      type: String,
      defualt: 'Video Element'
    },
    autoplay: {
      type: Boolean,
      defualt: true
    },
    loop: {
      type: Boolean,
      default: true
    },
    background: {
      type: Boolean,
      default: true
    },
    sources: {
      type: Array,
      required: true
    }
  },
  watch: {
    sources (nxt, prv) {
      this.changing = true

      setTimeout(() => {
        this.$el.load()
      }, ANIM_DELAY)

      setTimeout(() => {
        this.changing = false
      }, ANIM_DELAY * 2)
    }
  },
  mounted () {
    this.$el.addEventListener('playing', e => {
      this.$bus.$emit('video', { evt: 'begin' })
    })

    this.$el.addEventListener('ended', e => {
      this.$bus.$emit('video', { evt: 'end' })
    })
  },
  updated () {

  },
  render (vc) {
    return vc('video', {
      attrs: {
        id: this.id,
        poster: this.poster,
        autoplay: this.autoplay,
        loop: this.loop
      },
      style: this.background
        ? {
          ...BG_CSS,
          ...FADE_CSS(this.changing)
        }
        : {
          width: '100%',
          height: '100%',
          position: 'absolute',
          'z-index': -99,
          ...FADE_CSS(this.changing)
        }
    }, [
      this.sources.map(src => vc(vueSource, { props: { src } }))
    ])
  }
}

export default VueHtmlVideo
export {
}
