# App

Data visualization

### Prerequisites

Node

NPM

### Installing

Install all node dependencies

```bash
$ npm install
```

## Scripts

E.g

```bash
npm run start
```

| name | command | description |
| - | - |
| Dev Server | npm run start:dev | Starts the local development server |
| Build | npm run build | Compiles the project into the `dist` folder |
| Test | npm run test | Runs the Jest test runner onn all `*.test.js` files |
| Lint | npm run lint | Runs the `StandardJs` linter on all `*.js` files |

## Deployment

> TBC

## Development

**Progress**

Please refer to the `TODO` file for up to date development progress

[:link:]('./tood.md')

## Built With

* [Vue](https://vuejs.org/v2/api/) - Front-end framework
* [MoJs](https://github.com/legomushroom/mojs/blob/master/api/readme.md) - SVG Animation library
* [ReDom](https://github.com/legomushroom/mojs/blob/master/api/readme.md) - JS DOM Manipulation

## Authors

* **James Nicholls** - *Initial work* - [BitBucket](https://bitbucket.org/james-nicholls/)

## Acknowledgments

*
